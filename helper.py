import sys
import time
from screenutils import list_screens, Screen
from datetime import datetime

config = open(sys.path[0] + "/configs/tokenstash.cfg")
gsltf = open(sys.path[0] + "/configs/gslt.txt", "r")
log = open(sys.path[0] + "/configs/tokenUpdater.log", "a")
start = " "
log.write("helper running at " + datetime.now().strftime('%Y-%m-%d %H:%M:%S') + "\n")
gslt = gsltf.read().strip()

for line in config:
    if line.startswith("server_start"):
        sline = line.split()
        temp = sline[1:]
        tstring = ""
        for index in temp:
            tstring += index + " "
            start = tstring[1:-2]

server = False;
for s in list_screens():
    if s.name.strip() == "csgo":
        server = True
        log.write("csgo screen found \n")
if not server:
    c = Screen("csgo", False)
    q = Screen("quit")
    if not q.exists: q.initialize()
    q.send_commands("screen -dmS csgo")
    time.sleep(3)
    c.send_commands(start + " +sv_setsteamaccount " + gslt)    
#    c.send_commands("/home/steam/csgo/srcds_run srcdds -game csgo -console -usercon +game_type 0 +game_mode 1 +mapgroup mg_active +map de_dust2 -tickrate 128 +sv_setsteamaccount " + text + " -autoupdate -steam_dir /home/steam/ -steamcmd_script update_script.txt")

    log.write("csgo screen not found \n starting server \n")

gsltf.close()
log.write("\n\n")
log.close()

