import sys
import time
import urllib.request
from screenutils import list_screens, Screen
from datetime import datetime

config = open(sys.path[0] + "/configs/tokenstash.cfg")
gsltf = open(sys.path[0] + "/configs/gslt.txt", "r+")
log = open(sys.path[0] + "/configs/tokenUpdater.log", "a")
steamid = " "
apikey = " "
serverkey = " "
gslt = " "
start = " "
hostname = " "
endpoint = " "

def startServer(q, c):
    if not c.exists: c.initialize()
    log.write("start is: " + start + "\n")
    c.send_commands(start + " +sv_setsteamaccount " + gslt)
#    c.send_commands("/home/steam/csgo/srcds_run srcdds -game csgo -console -usercon +game_type 0 +game_mode 1 +mapgroup mg_active +map de_dust2 -tickrate 128 +sv_setsteamaccount " + gslt + " -autoupdate -steam_dir /home/steam/ -steamcmd_script update_script.txt")



def stopServer(q, c):
    if not q.exists: q.initialize()
    if not c.exists: c.initialize()    
    q.send_commands("killall -w -TERM srcds_linux")
    q.send_commands("date")
    time.sleep(3)
    c.kill()


log.write("Token Updater running at " + datetime.now().strftime('%Y-%m-%d %H:%M:%S') + "\n")
for line in config:
    if line.startswith("tokenstash_steamid"):
        sline = line.split()
        steamid = sline[1][1:-1]

    if line.startswith("tokenstash_apikey"):
        sline = line.split()
        apikey = sline[1][1:-1]

    if line.startswith("tokenstash_serverkey"):
        sline = line.split()
        serverkey = sline[1][1:-1]

    if line.startswith("server_start"):
        sline = line.split()
        temp = sline[1:]
        tstring = ""
        for index in temp:
            tstring += index + " "
            start = tstring[1:-2]

    if line.startswith("hostname"):
        sline = line.split()
        hostname = sline[1][1:-1]

    if line.startswith("endpoint"):
        sline = line.split()
        endpoint = sline[1][1:-1]
        
log.write("Calling Token Stash API \n")
url = "http://api.tokenstash.com/gslt_getservertoken.php?version=0.09&steamid=" + steamid + "&apikey=" + apikey + "&serverkey=" + serverkey + "&hostname=" + hostname + "&endpoint=" + endpoint
request = urllib.request.Request(url)
site = urllib.request.urlopen(request).read().decode('utf-8')
#Parse Token Stash web page
if site == "NO_TOKEN": log.write("NO TOKEN:Go Generate more tokens")
if site.startswith("SERVER_TOKEN"):
    gslt = site.split()[1].strip()
    text = gsltf.read().strip()
    #Token Current
    if text == gslt.strip(): log.write("Token is current")
    #New Token Found
    else:
        gsltf.seek(0)
        gsltf.write(gslt.strip())
        log.write("Token updated from: " + text + " To: " + gslt + "\n")
        #Create screens
        c = Screen("csgo")
        q = Screen("quit")
        #Create screen session if not already running
        if not c.exists: c.initialize()
        if not q.exists: q.initialize()
        stopServer(q, c)
        time.sleep(5)
        startServer(q, c)
        log.write("Server Started \n")        
        
log.write(" \n\n")
config.close()
gsltf.close()
log.close()
